﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ArtSharedDll
{
	public class TerrainGrid : MonoBehaviour 
	{


		[HideInInspector]
		public float gridSize = 1f;
		[HideInInspector]
		public int gridCount = 128;
		[HideInInspector]
		public List<GridData>gridDatas = new List<GridData>();

		[Range(1,20)]
		public int brushSize = 1;

		private Terrain mTerrain 
		{
			get{
				return transform.GetComponentInChildren<Terrain>();
			}
		}


		private int brushIndex = -1;


		void OnValidate()
		{ 
			ClearBrush();
		}

		public void Init()
		{
			TerrainData terrainData = this.mTerrain.terrainData;

			float [, ,] splatmapData = new float[terrainData.alphamapWidth, terrainData.alphamapHeight, terrainData.alphamapLayers];

			int count = terrainData.alphamapWidth;

			int allCount = count * count;

			if(gridDatas.Count > allCount)
			{
				gridDatas.RemoveRange(allCount,gridDatas.Count - allCount);
			}
			for ( int x = 0; x < count; x++ )
			{
				for ( int y = 0; y < count; y++ )
				{
					int index = (x * count) + y;
					if(gridDatas.Count<=index)
					{
						gridDatas.Add(new GridData());
					}

					if(gridDatas[index].type == GridType.Empty)
					{
						splatmapData [x, y, 0] = 0f;
						splatmapData [x, y, 1] = 1f;
						splatmapData [x, y, 2] = 0f;
					} else if(gridDatas[index].type == GridType.Select)
					{
						splatmapData [x , y, 0] = 1f; 
						splatmapData [x , y, 1] = 0f; 
						splatmapData [x , y, 2] = 0f; 
					}

				}
			}

			terrainData.SetAlphamaps( 0, 0, splatmapData );

		}


		private GridType cacheType = GridType.Select;
		public void CacheDown(Vector3 postion)
		{
			int index =GetIndexByPos(postion);
			if(gridDatas.Count >index)
			{
				cacheType = gridDatas[index].type  == GridType.Empty?GridType.Select:GridType.Empty;
			}
		}



		public void Click(Vector3 postion)
		{
			int index =GetIndexByPos(postion);
			if(gridDatas.Count >index)
			{
				setGridType(index,gridDatas[index].type== GridType.Empty?GridType.Select:GridType.Empty);
				Refresh(index,false);
			}
		}

		public void Drag( Vector3 postion)
		{
			int index =GetIndexByPos(postion);
			if(gridDatas.Count >index)
			{
				setGridType(index,cacheType);
				Refresh(index,false);
			}
		}

		public void Brush(Vector3 postion)
		{
			
			int index = GetIndexByPos(postion);
		
			if(index != brushIndex)
			{
				
				if(brushIndex !=-1)
				{
					Refresh(brushIndex,false);
				}
				Refresh(index,true);
				brushIndex =index ;
			}
		}


		public void ClearBrush()
		{
			if(brushIndex != -1)
			{
				Refresh(brushIndex,false,20);
				brushIndex = -1;
			}

		}


		private int GetIndexByPos(Vector3 postion)
		{
			return (Mathf.FloorToInt(postion.z/gridSize) *Mathf.CeilToInt((gridCount *gridSize) /gridSize) ) +  Mathf.FloorToInt(postion.x  /  gridSize);

		}

		private void GerArea(int index,out int minX, out int minY,out int maxX,out int maxY)
		{
			int size = ((brushSize *2) - 1 ) / 2;

			int y = (int)(index/gridCount);
			int x = index - (y * (gridCount -1)) - y;

			int xMIn = y / gridCount;
			int xMax = xMIn + gridCount;


			int yMin = 0;
			int yMax = gridCount;


			minX = Mathf.Max(xMIn,x - size);
			minY = Mathf.Max(yMin,y - size);

			maxX = Mathf.Min(xMax,x + Mathf.Max(size,1));
			maxY = Mathf.Min(yMax,y + Mathf.Max(size,1));
		}


		private List<int>GetIndexs(int index)
		{
			int minX =0;
			int minY =0;
			int maxX =0;
			int maxY =0;

			GerArea(index,out minX, out  minY,out  maxX,out  maxY); 

			List<int> refresh = new List<int>();
			for(int y =minY; y< maxY; y++)
			{
				for(int x =minX; x<  maxX; x++)
				{
					refresh.Add((y * gridCount) + x);
				}
			}
			return refresh;
		}

		private void setGridType(int index , GridType type)
		{

		 	List<int> indexs = GetIndexs(index);

			for(int i=0; i< indexs.Count; i++)
			{
				gridDatas[indexs[i]].type = type;
			}

		}

		private void Refresh(int  index , bool brush)
		{
			Refresh(index,brush,brushSize);
		}
		private void Refresh(int  index , bool brush,int brushSize)
		{
			int minX =0;
			int minY =0;
			int maxX =0;
			int maxY =0;

			GerArea(index,out minX, out  minY,out  maxX,out  maxY); 


			int width = Mathf.Max (maxX - minX , 1);
			int height = Mathf.Max (maxY - minY, 1);
			TerrainData terrainData = this.mTerrain.terrainData;

			float[,,] splatmapData = terrainData.GetAlphamaps(minX, minY, width, height);
			for (int i = 0; i < height; i++)
			{
				for (int j = 0; j < width; j++)
				{
					int ox = i+minY;
					int oy = j+minX;

					int offset = (ox* gridCount) + oy;
					if(brush)
					{
						splatmapData [i, j, 0] = 0f;
						splatmapData [i, j, 1] = 0f;
						splatmapData [i, j, 2] = 1f;
					}else
					{
						if(gridDatas[offset].type == GridType.Empty)
						{
							splatmapData [i, j, 0] = 0f;
							splatmapData [i, j, 1] = 1f;
							splatmapData [i, j, 2] = 0f;
						} else if(gridDatas[offset].type == GridType.Select)
						{
							splatmapData [i, j, 0] = 1f;
							splatmapData [i, j, 1] = 0f;
							splatmapData [i, j, 2] = 0f;
						}

					}

				}
			}
			terrainData.SetAlphamaps(minX, minY, splatmapData);

		}


		private void Refresh(List<int> indexs,bool brush = false)
		{

			TerrainData terrainData = this.mTerrain.terrainData;
			if(terrainData)
			{
				for(int i =0; i<indexs.Count; i++ )
				{
					int index = indexs[i];
					int y = index/gridCount;
					int x = index - (y * (gridCount -1)) - y;
					if(gridDatas.Count >index)
					{
						float [, ,] splatmapData = terrainData.GetAlphamaps(x,y,1,1);
						if(gridDatas[index].type == GridType.Empty)
						{
							
							splatmapData [0, 0, 0] = 0f;
							splatmapData [0, 0, 1] = 1f;
							splatmapData [0, 0, 2] = 0f;
						} else if(gridDatas[index].type == GridType.Select)
						{
							splatmapData [0, 0, 0] = 1f;
							splatmapData [0, 0, 1] = 0f;
							splatmapData [0, 0, 2] = 0f;
						}

						if(brush)
						{
							splatmapData [0, 0, 0] = 0f;
							splatmapData [0, 0, 1] = 0f;
							splatmapData [0, 0, 2] = 1f;
						}

						terrainData.SetAlphamaps( x, y, splatmapData );
					
					}
				}
			}
		}



		[System.Serializable]
		public class GridData
		{
			public GridType type = GridType.Select;
		}


		public enum GridType
		{
			Select =1,
			Empty

		}
	}
}